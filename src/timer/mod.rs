/*
 *  PopKorn Speedrun Splitter: Timer Module
 *  ---------------------------------------
 *  by: BlacKernel
 *
 * 
 *          This module contains the underlying timer backend so that you can build whatever
 *          frontend up from it that you would want.
 *
 *          It contains:
 *              Start/Split/Stop functionality
 *              Global Hotkeys (Thanks to livesplit-hotkey)
 *              Yaml file integration (Thanks to yaml-rust)
 *              And it should be easy for beginners to work with.
 *
 */
 
// External crates
extern crate yaml_rust;     //-> To process yaml-files
extern crate livesplit_hotkey;  //-> To use global hotkey functionality
extern crate time;  //-> To store time data with at least millisecond presicion

use time;

mod imports;
mod structs;

pub mod yaml;
pub mod hotkeys;
pub mod splitter;
