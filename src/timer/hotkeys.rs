// Internal modules
use super::structs;                    //-> To get access to common structs. Namely: Run

// External macros
#[macro_use]
extern crate lazy_static;       //-> Because livesplit-hotkey needs things to have static scope

// std imports
use std::sync::Mutex;           //-> To make a mutably accessable lazily static variable

// livesplit-hotkey imports
use livesplit_hotkey::linux::*; //-> To use linux keybindings

// Statics
lazy_static! { pub static ref GAME: Mutex<Run> = Mutex::new( structs::Run::empty() ); } //-> The psudo-static quasi-mutable Mutex variable thing that will hold our run info 
pub static HOTKEY_START:    KeyCode = KeyCode::Left;                                    //-> The keybinding to start the run (DEFAULT: Left )
pub static HOTKEY_SPLIT:    KeyCode = KeyCode::Right;                                   //-> The keybinding to split the run (DEFAULT: Right )
pub static HOTKEY_STOP:     KeyCode = KeyCode::Down;                                    //-> The keybinding to end the run (DEFAULT: Down )
pub static HOTKEY_PAUSE:    KeyCode = KeyCode::Up;                                      //-> The keybinding to pause the run (DEFAULT: Up )


// IN THEORY this will paste into the main.rs when imported, causing it to start this function and
// then start the main function in main.rs... IN THEORY
#![feature(start)]
#[start]
fn setup() {
    
    //  Set up the keystroke hook 
    let hook = Hook::new().unwrap();

    // Register the hotkeys
    hook.register( HOTKEY_START,    || GAME.lock().unwrap().start_run()     ).unwrap();
    hook.register( HOTKEY_SPLIT,    || GAME.lock().unwrap().split()         ).unwrap();
    hook.register( HOTKEY_STOP,     || GAME.lock().unwrap().end_run()       ).unwrap();
    hook.register( HOTKEY_PAUSE,    || GAME.lock().unwrap().pause_toggle()  ).unwrap();

    // Start the main function in main.rs
    main();

}
