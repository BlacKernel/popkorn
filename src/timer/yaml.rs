/*
 *  PopKorn Speedrun Splitter: Yaml Injestor
 *  ----------------------------------------
 *  by: BlacKernel
 *
 *      This module contains the nessesary code to process yaml files into the native
 *      Run structs of the splitter
 */

// Internal modules
//mod structs;                //-> Contains the structs used for the splitter. Namely: Run, Split
//mod imports;                //-> Contains the universal imports for the splitter. Namely: time
use super::structs;
use super::imports;

// std imports
use std::fs::File;          //-> To open the raw yaml file
use std::io::prelude::*;    //-> To feed the raw yaml file into a string

// yaml-rust imports
use yaml_rust::yaml;        //-> To process yaml files and create yaml::Yaml objects

//  This function takes in
//      A file name string (e.g. game.yaml, /home/onyxus/split-files/game.yaml, etc.)
//  And returns
//      A usable yaml::Yaml object
pub fn yaml_injest( file_name: &imports::string::String ) -> yaml::Yaml {

    // Open the file file_name...
    let mut f = File::open( file_name ).unwrap();

    // Make a dummy string to hold the contents...
    let mut s = imports::string::String::new();

    // Shove the contents into the string...
    f.read_to_string( &mut s ).unwrap();

    // Wrap it in a yaml::Yaml object...
    let game_yaml = &yaml::YamlLoader::load_from_str(&s).unwrap()[0];

    // Ship it for Christmas.
    return game_yaml;
}

// This struct contains the run information for the game
impl structs::Run {

    //  This function takes in
    //      A reference to a mutable self
    //      A yaml::Yaml object
    //  And does
    //      Destructively modify the self's Run data with
    //      the data from the yaml::Yaml object
    pub fn load_from_yaml( &mut self, game_yaml: &yaml::Yaml ) {
        
        ////    ADD THE INFO OF THE SPLITS TO A VEC ////

        // This temporary vector will contain our splits
        let mut tmp_vec = Vec::new();

        // For loops don't work well with yaml_rust arrays,
        // but they don't panic! when you overindex, so
        // you can while loop through and wait for it to gracefully
        // yell at you.
        let mut i = 0;
        while !game_yaml["splits"][i].is_badvalue() {
            
            // Make a temporary split with all of the info
            // of the split from the yaml file.
            let tmp_split = structs::Split {
            
                // The name of the split
                name: game_yaml["splits"][i]["name"].as_str().unwrap().to_string(),

                // The offset (positive for more handicap, negitive for less)
                offset: imports::time::Duration::milliseconds( game_yaml["splits"][i]["offset"].as_i64().unwrap() ),

                // The best time you want to be compared to (For the split)
                best_time: imports::time::Duration::milliseconds( game_yaml["splits"][i]["best"].as_i64().unwrap() ),

                // The duration of this split (Which for now is 0, since we haven't done it yet)
                split_dur: imports::time::Duration::milliseconds( 0 ),

                // The pause start time (Which for now is empty, since we aren't paused)
                pause_start: imports::time::empty_tm(),

                // The pause flag (Which for now is false, since we aren't paused)
                is_paused: false,
            };

            // Push the temporary split onto the temporary vector
            tmp_vec.push(tmp_split);

            // Advance the accumulator
            i += 1;
        }

        //// ADD THE INFO OF THE GAME TO THE RUN ////

        // The name of the game
        self.name = game_yaml["name"].as_str().unwrap().to_string();

        // The category of the game
        self.category = game_yaml["category"].as_str().unwrap().to_string();

        // The best time you want to be compared to (For the whole run)
        self.best_time = imports::time::Duration::milliseconds( game_yaml["best"].as_i64().unwrap() );

        // The splits of the run (( TAKEN FROM THE TEMPORARY VECTOR ABOVE ))
        self.splits = tmp_vec;
    }
}
