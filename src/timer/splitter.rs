use super::imports;
use super::structs;

use std::io::{self, Write}; //-> To make the terminal output look a little prettier

impl Run {

    //  This function takes in
    //      Nothing
    //  And does
    //      Create an  empty Run
    pub fn empty() -> Self {
		Self {
			name:           imports::string::String::new(),
			category:       imports::string::String::new(),
			best_time:      imports::time::Duration::milliseconds(0),
			run_dur:        imports::time::Duration::milliseconds(0),
			splits:         Vec::new(),
			current_split:  0,
			start_time:     imports::time::empty_tm(),
			is_running:     false,
			is_time_to_die: false,
		}
    }

    //  This function takes in
    //      A reference to a mutable self
    //  And does
    //      Start the run
    pub fn start_run( &mut self ) {
    
        // If we are NOT currently paused and we are NOT currently in a run...
        if !self.splits[ self.current_split ].is_paused && !self.is_running {
        
            //...we can start the run. (Otherwise, it skips over the whole function)
            
            //  Set the start time equal to now
            self.start_time = imports::time::now();

            //  The starting split is the 0th one
            self.current_split = 0;

            //  Start the run
            self.is_running = true;

            //  Print out
            //  Run of GAME NAME GAME CATEGORY started at NAME OF DAY DAY OF MONTH NAME OF MONTH 24HR TIME WITH SECONDS...
            //      NAME OF 0th SPLIT: 
            print!("Run of {} {} started at {}...\n\t{}: ", self.name, self.category, time::strftime( "%a %d %b %T", &self.start_time ).unwrap(), self.splits[ self.current_split ].name );
            io::stdout().flush().unwrap();
        }
    }

    //  This function takes in
    //      A reference to a mutable self
    //  And does
    //      Split the run
    pub fn split( &mut self ) {}

    //  This function takes in
    //      A reference to a mutable self
    //  And does
    //      End the run
    pub fn end_run( &mut self ) {}

    //  This function takes in
    //      A reference to a mutable self
    //  And does
    //      Pause/Unpause the run
    pub fn pause_toggle( &mut self ) {}

}
