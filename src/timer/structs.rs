/*
 *  PopKorn Speedrun Splitter: Common Structs
 *  -----------------------------------------
 *  by: BlacKernel
 *
 *      This module contains structs common to more than one other module
 */

// Internal modules
use super::imports;    //-> To grab common imports. Namely: time, std::string
use super::time;

// The Run struct contains all of the data for a speedrun
// It consists of:
pub struct Run {
    name:           imports::string::String,    // The name of the game,
    category:       imports::string::String,    // The category of the speedrun (Any%, Glitchless, etc),
    best_time:      imports::time::Duration,    // The best time you want to be compared to (For the whole run),
    run_dur:        Duration,    // The duration of the run to this point
    splits:         Vec<Splits>,                // the Splits (defined below) of a Run, orginized in a vector,
    current_split:  usize,                      // The index of the split you are currently in,
    start_time:    	time::Tm,          // The time the run started,
    is_running:     bool,                       // A bool to tell the program if we are in the middle of a run,
    is_time_to_die: bool,                       // A bool to tell the program if it is time to end the program,
}

// The Split struct contains all of the data for a split of a speedrun
// It consists of:
pub struct Split {
    name:           imports::string::String,    // The name of this split,
    offset:         imports::time::Duration,    // The offset for this split (Positive for more positive handicap, negitive for negitive handicap),
    best_time:      imports::time::Duration,    // The best time you want to be compared to (For this split),
    split_dur:      imports::time::Duration,    // The duration of this split,
    pause_start:    imports::time::Tm,          // The time you started pausing,
    is_paused:      bool,                       // A bool to tell the program if we are paused

}
