mod timer;

use std::env;

fn main() {
    let args: Vec<_> = env::args().collect();
    let game_yaml = timer::yaml::yaml_injest( &args[1] );

    timer::hotkeys::GAME.lock().unwrap().load_from_yaml( game_yaml );

    println!("PopKorn Splitter: Ready to begin {} run",
             timer::hotkeys::GAME.lock().unwrap().name);

    println!("---------------------------------------");

    while !timer::hotkeys::GAME.lock().unwrap().is_time_to_die {}
}
